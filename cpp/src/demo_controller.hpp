#pragma once

#include <vector>

#include "gen/demo_controller_observer.hpp"
#include "gen/demo_controller.hpp"
#include "gen/demo_data.hpp"

using namespace std;

namespace demo {
    
    class demo_controller_impl: public demo_controller, public enable_shared_from_this<demo_controller_impl> {
        public:
            void subscribe(const shared_ptr<demo_controller_observer>& observer) override;
            void fetch() override;
            
        private:
            shared_ptr<demo_controller_observer> observer_;
    };
    
}