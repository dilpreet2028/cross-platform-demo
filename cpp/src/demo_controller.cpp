#include "demo_controller.hpp"

#include <json11.hpp>
#include <unordered_map>

#include "gen/demo_controller_observer.hpp"
#include "gen/demo_controller.hpp"
#include "gen/demo_data.hpp"

using namespace json11;
using namespace std;
namespace demo {

    shared_ptr<demo_controller> demo_controller::create() {
        return make_shared<demo_controller_impl>();
    }

    void demo_controller_impl::subscribe(const shared_ptr<demo_controller_observer>& observer) {
         observer_ = observer; 
    }

    void demo_controller_impl::fetch() {

        Json my_json = Json::object {
            { "title", "This is a title" },
            { "body", "This is body" },
        };
        string data = my_json.dump();
        string err;
        auto json = json11::Json::parse(data, err);
        auto title = json["title"].string_value();
        auto body = json["body"].string_value();
        demo_data demo = {title, body};
        observer_->on_update(demo);
        
    }

}