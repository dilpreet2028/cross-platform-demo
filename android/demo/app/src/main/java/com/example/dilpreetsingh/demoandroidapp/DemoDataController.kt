package com.example.dilpreetsingh.demoandroidapp

import android.util.Log

class DemoDataController: DemoControllerObserver() {

    val controller = DemoController.create()

    init {
        controller.subscribe(this)
    }

    fun fetch() {
        controller.fetch()
    }

    override fun onUpdate(viewData: DemoData?) {
        Log.d("mytag", viewData.toString())
    }

}