package com.example.dilpreetsingh.demoandroidapp

import android.app.Application
import android.util.Log

class MainApplication: Application() {

    init {
        System.loadLibrary("demo_android")
    }

    override fun onCreate() {
        super.onCreate()
    }
}