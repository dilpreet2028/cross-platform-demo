// AUTOGENERATED FILE - DO NOT MODIFY!
// This file generated by Djinni from demo.djinni

#include "NativeDemoController.hpp"  // my header
#include "NativeDemoController.hpp"
#include "NativeDemoControllerObserver.hpp"

namespace djinni_generated {

NativeDemoController::NativeDemoController() : ::djinni::JniInterface<::demo::demo_controller, NativeDemoController>("com/example/dilpreetsingh/demoandroidapp/DemoController$CppProxy") {}

NativeDemoController::~NativeDemoController() = default;


CJNIEXPORT void JNICALL Java_com_example_dilpreetsingh_demoandroidapp_DemoController_00024CppProxy_nativeDestroy(JNIEnv* jniEnv, jobject /*this*/, jlong nativeRef)
{
    try {
        DJINNI_FUNCTION_PROLOGUE1(jniEnv, nativeRef);
        delete reinterpret_cast<::djinni::CppProxyHandle<::demo::demo_controller>*>(nativeRef);
    } JNI_TRANSLATE_EXCEPTIONS_RETURN(jniEnv, )
}

CJNIEXPORT jobject JNICALL Java_com_example_dilpreetsingh_demoandroidapp_DemoController_create(JNIEnv* jniEnv, jobject /*this*/)
{
    try {
        DJINNI_FUNCTION_PROLOGUE0(jniEnv);
        auto r = ::demo::demo_controller::create();
        return ::djinni::release(::djinni_generated::NativeDemoController::fromCpp(jniEnv, r));
    } JNI_TRANSLATE_EXCEPTIONS_RETURN(jniEnv, 0 /* value doesn't matter */)
}

CJNIEXPORT void JNICALL Java_com_example_dilpreetsingh_demoandroidapp_DemoController_00024CppProxy_native_1subscribe(JNIEnv* jniEnv, jobject /*this*/, jlong nativeRef, jobject j_observer)
{
    try {
        DJINNI_FUNCTION_PROLOGUE1(jniEnv, nativeRef);
        const auto& ref = ::djinni::objectFromHandleAddress<::demo::demo_controller>(nativeRef);
        ref->subscribe(::djinni_generated::NativeDemoControllerObserver::toCpp(jniEnv, j_observer));
    } JNI_TRANSLATE_EXCEPTIONS_RETURN(jniEnv, )
}

CJNIEXPORT void JNICALL Java_com_example_dilpreetsingh_demoandroidapp_DemoController_00024CppProxy_native_1fetch(JNIEnv* jniEnv, jobject /*this*/, jlong nativeRef)
{
    try {
        DJINNI_FUNCTION_PROLOGUE1(jniEnv, nativeRef);
        const auto& ref = ::djinni::objectFromHandleAddress<::demo::demo_controller>(nativeRef);
        ref->fetch();
    } JNI_TRANSLATE_EXCEPTIONS_RETURN(jniEnv, )
}

}  // namespace djinni_generated
