//
//  ViewController.swift
//  Demo
//
//  Created by Dilpreet Singh on 20/06/18.
//  Copyright © 2018 Dilpreet Singh. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    private let source = ViewDataController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.source.fetch()
    }


}

