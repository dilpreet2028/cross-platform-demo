//
//  ViewDataController.swift
//  Demo
//
//  Created by Dilpreet Singh on 20/06/18.
//  Copyright © 2018 Dilpreet Singh. All rights reserved.
//

import Foundation

class ViewDataController {
    
    private let controller = CPDemoController.create()!
    
    init() {
        controller.subscribe(self)
    }
    
    func fetch() {
        controller.fetch()
    }
    
    
}

extension ViewDataController: CPDemoControllerObserver {
    func onUpdate(_ viewData: CPDemoData) {
        NSLog(viewData.title)
        NSLog(viewData.body)
    }
    
}
